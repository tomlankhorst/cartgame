/* 
 * NodeHID Server
 * @author Tom Lankhorst (info@tomlankhorst.nl)
 */

var sys     = require("sys");
var http    = require("http");
var qs      = require('querystring');
var HID     = require('node-hid');
var WebSocket = require('ws');
var util    = require('util');

var device = null;

var WebSocketServer = require('ws').Server
  , wss = new WebSocketServer({port: 8081});
wss.on('connection', function(ws) {
    
    sys.puts("Incoming websocket");
    
    ws.on('message', function(message) {
        message = JSON.parse(message);
        
        switch(message.method){
            case 'setDevice':
                sys.puts("Selected Device: "+message.data.path);
                if(device === null){
                    device = new HID.HID(message.data.path);
                    device.on('data', function(data){
                        wss.broadcast(JSON.stringify({
                            position: data,
                            caller: "position"
                        }));
                    });
                }
                
                break;
            case 'setImpedance': 
                
                var buffer = new Buffer(17); 
                buffer.fill(0);
                                        
                buffer.writeInt32LE(message.data.J, 1);
                buffer.writeInt32LE(message.data.B, 5);
                buffer.writeInt32LE(message.data.K, 9);
                buffer.writeInt32LE(message.data.r, 13);
                 
                device.write(buffer);
                
                break;
        }
    });
    
});
wss.broadcast = function(data) {
    for(var i in this.clients)
        this.clients[i].send(data);
};

http.createServer(function(request,response){  
    sys.puts("Incoming request");
    sys.puts(request.url);
    
    if(typeof routes[request.url] !== "undefined"){
        routes[request.url](request,response);
    } else {
        routes["404"](request,response);
    }
    
    
}).listen(8080);  

sys.puts("Server Running on 8080");   
sys.puts("Websockets Running on 8081");   

var routes = {
    '404' : function(request, response){
        response.writeHeader(404, {
            "Content-Type": "text/plain",
            "Access-Control-Allow-Origin": "http://localhost:8383"
        }); 
        response.write("Route not found");  
        response.end(); 
    },
    '/getDevices': function(request, response){
        response.writeHeader(200, {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "http://localhost:8383"
        }); 
        var responseData = {
            devices: HID.devices()
        };
        response.write(JSON.stringify(responseData));  
        response.end(); 
    }
};

