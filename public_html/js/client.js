/* 
 * NodeHID Client
 * @author Tom Lankhorst (info@tomlankhorst.nl)
 */

var plotterJ, plotterK, plotterB, plotterr;

var options = {
    url: 'http://localhost:8080/',
    websocket: 'ws://localhost:8081/'
};
var selectedDevice = null;

function init_client(){
    get_devices();
}

window.addEvent('domready', init_client);

var websocket = null;
function open_websocket(){
    
    set_status('Connecting...');
    
    if(websocket)
        close_websocket();
    
    if(selectedDevice === null)
        return;
    
    // websocket
    websocket = websocket = new WebSocket(options.websocket);
    websocket.onopen = function(evt) { set_status('Connected'); websocket.send(JSON.stringify({
            method: "setDevice",
            data: {
                path: selectedDevice.path
            }
    }));

    myGame = new Game({
        containerId: 'game'
    });
    
    plotterJ = new Plotter('impPlotJ', {range: [-0.001, 0.001]});
    plotterB = new Plotter('impPlotB', {range: [-0.12, 0.12]});
    plotterK = new Plotter('impPlotK', {range: [-5, 5]});
    plotterr = new Plotter('impPlotr', {range: [-8000, 8000]});
    
    setInterval(function(){
        plotterJ.pushData(impedance_state[0]);
        plotterB.pushData(impedance_state[1]);
        plotterK.pushData(impedance_state[2]);
        plotterr.pushData(impedance_state[3]);
    }, 50);
    
}; 
    websocket.onclose = function(evt) { set_status('Disconnected');  }; 
    websocket.onmessage = function(evt) { websocketResponse(evt); }; 
    websocket.onerror = function(evt) { };
}

function close_websocket() {
    websocket.close();
}

function get_devices(){
    jQuery.getJSON(options.url + 'getDevices', function(data){
        jQuery('#deviceSelect').empty();
        jQuery('#deviceSelect').append(jQuery('<option value="">-- Select a device --</option>').data('deviceData', false));
        jQuery.each(data.devices, function(){
            jQuery('#deviceSelect').append(jQuery('<option value="">'+this.manufacturer+' - '+this.product+'</option>').data('deviceData', this));
        });
        jQuery('#deviceSelect').change(function(){
            selectedOption      = jQuery(this).find('option:selected');
            deviceData          = selectedOption.data('deviceData');
            if(deviceData){
                selectedDevice = deviceData;
                open_websocket();
            } else {
                selectedDevice = null;
                close_websocket();
            }
        });
    });
}


function set_status(msg){
    jQuery('#status').text(msg);
}

var wsQueue = {};

function websocketRequest(method, data, success){
    websocket.send(JSON.stringify({
        method: method,
        data: data
    }));
    wsQueue[method] = success;
};

function websocketResponse(evt){
    
    data  = JSON.parse(evt.data);
    if(typeof wsQueue[data.caller] !== "undefined"){
        fn = wsQueue[data.caller];
        fn(data);
        delete wsQueue[data.caller];
    }
    
    if(data.caller === "position"){
        handlePosition(data.position);
    }
    
}

var position = null;
var initgame = true;
function handlePosition(bytes){
        
    position = 0;
    for (var i = 0; i < 4; ++i) {        
        position += bytes[i];        
        if (i < 3) {
            position = position << 8;
        }
    }
   
    if(myGame)
        myGame.handlePosition(position);
    
    
}

var impedance_state = [0, 0, 0, 0];

function setImpedance(J, B, K, r)
{
    
    if(impedance_state[0] !== J || impedance_state[1] !== B || impedance_state[2] !== K || impedance_state[3] !== r){
        // impedance changes, send it
        impedance_state = [J, B, K, r];
        websocket.send(JSON.stringify({
            method: 'setImpedance',
            data: {
                J: Math.round(J*1000000),
                B: Math.round(B*1000000),
                K: Math.round(K*1000000),
                r: r
            }
        }));
    }
}

var Plotter = new Class({
    Implements:     Options,
    container:      null,
    canvas:         null,
    ctx:            null,
    height:         0,
    width:          0,
    options:        {
        width:      60,
        range:      [-1, 1]
    },
    data:    [],
    initialize:     function(id, options)
    {
        
        this.setOptions(options);
        
        for(var i=0; i<this.options.width; i++)
            this.data.push(0);
        
        this.container  = $(id);
        this.container.addClass('plottercanvas');
        this.width      = this.container.getSize().x*0.6;
        this.height     = Math.round(this.width*0.66);
        this.canvas     = new Element('canvas', {
            styles: {
                width:  this.width,
                height: this.height
            }
        });
        this.container.adopt(this.canvas);
        this.ctx        = this.canvas.getContext('2d');
        
        this.draw();
        
    },
    pushData:   function(value)
    {
        this.data.push(value);
        this.data.shift();
        this.draw();
    },
    draw:       function()
    {
        this.clear();
        
        var xWidth  = this.canvas.width/this.options.width;
        
        this.ctx.strokeStyle = "black 2px";
        
        this.ctx.beginPath();
        
        this.ctx.moveTo(0, this.canvas.height*(this.data[0]-this.options.range[0])/(this.options.range[1]-this.options.range[0]));
        for (i = 1; i < this.options.width; i++) {
           this.ctx.lineTo(i*xWidth, this.canvas.height*(this.data[i]-this.options.range[0])/(this.options.range[1]-this.options.range[0]));
        }
        
        this.ctx.stroke();
        
    },
    clear:      function()
    {
        this.ctx.clearRect(0,0,this.canvas.width, this.canvas.height);
    }
});