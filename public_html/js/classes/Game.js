var Game = new Class({
    
    Implements: [Options],
    
    stage:          null,
    parent:         null,
    player:         null,
    environment:    null,
    anim:           null,
    
    staticLayers:   {},
    animLayers:     {},
    background:     null,
    
    options: {
        containerId:    'game',
        width:          800,
        height:         600,
        pixelRatio:     1
    },
    
    initialize: function(options){
        this.setOptions(options);
                
        this.game = $(this.options.containerId);
        this.stage  = new Kinetic.Stage({
            container:      this.game.get('id'),
            width:          this.options.width,
            height:         this.options.height
        }); 
        
        this.staticLayers.background    = new Kinetic.Layer();
        
        this.staticLayers.background.add(new Kinetic.Rect({
            fill: '#ccc',
            width: this.options.width,
            height: this.options.height
        }));
        
        this.animLayers.environment     = new Kinetic.Layer();
        this.animLayers.player          = new Kinetic.Layer();
        
        this.player         = new Player(this);
        this.environment    = new Environment(this);
        
        Object.each(this.staticLayers, function(layer, key){
            this.stage.add(layer);
        }, this);
        
        Object.each(this.animLayers, function(layer, key){
            this.stage.add(layer);
        }, this);
        
        this.anim   = new Kinetic.Animation(this.draw.bind(this), [this.animLayers.environment, this.animLayers.player]);
        this.anim.start();
        
    },
    
    positionSetpoint: false,
    
    handlePosition: function(position)
    {
        if(this.positionSetpoint === false){
            this.positionSetpoint = position;
            this.setImpedance(0,0,1,0);
        }
        
        var internalPosition = position - this.positionSetpoint;
        
        this.player.steer(internalPosition/8000);
        
    },
    
    setImpedance: function(J,B,K,r){
        setImpedance(J,B,K,r+this.positionSetpoint);
    },
    
    setBackgroundFill: function(fill)
    {
        this.staticLayers.background.children[0].fill(fill).draw();
    },
    
    draw: function(frame)
    {
        // draw the environment
        this.environment.draw(frame);
        this.player.draw(frame);
        
    }
    
});