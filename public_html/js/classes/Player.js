var Player = new Class({
    
    Extends: Drawable,
    
    position: {
        x: 0,
        y: 0
    },
    target: {
        x: 0,
        y: 0
    },
    dimensions: {
        w: 60,
        h: 80
    },
    steering: 0,
    
    speed: [0.5, 0.5, 0.1, 0.1], // pixel/ms -x x y -y
    steeringspeed: 10,
    
    image: null,
    
    initialize: function(game){
        
        this.parent(game);
        this.position.x = this.target.x = Math.round(this.game.options.width/2 - this.dimensions.w/2);
        this.position.y = this.target.y = Math.round(this.game.options.height - this.dimensions.h*2);
        var imageObj = new Image();
        imageObj.onload = (function() {
            this.image = new Kinetic.Image({
                x: 0,
                y: 0,
                offset: {x: Math.round(this.dimensions.w/2), y:Math.round(this.dimensions.h/2)},
                image: imageObj,
                width: this.dimensions.w,
                height: this.dimensions.h
            });
            this.game.animLayers.player.add(this.image);
        }).bind(this);
        imageObj.src = 'img/player.png';
        
    },
    
    move: function(to)
    {
        this.target.x = this.game.options.width*to;
    },
    
    steer: function(amount)
    {
        this.steering = amount;
    },
    
    draw: function(frame)
    {
        // delta time
        delta = this.delta(frame.time);
        // wait for the image to be downloaded
        if(!this.image)
            return;
        
        var steeringspeed = this.steeringspeed * this.game.environment.speed;
        this.target.x+=this.steering*delta*steeringspeed;
        
        deltax = this.target.x - this.position.x;
        deltay = this.target.y - this.position.y;
        
        if(deltax > 0){
            var xmove = Math.min(this.speed[0]*delta, deltax);
            this.position.x+= xmove;
            this.image.setRotation(xmove/steeringspeed/delta*90);
        }
        else if(deltax < 0){
            var xmove = Math.max(-this.speed[1]*delta, deltax);
            this.position.x+= xmove;
            this.image.setRotation(xmove/steeringspeed/delta*90);
        } else {
            this.image.setRotation(0);
        }
        if (deltay > 0)
            this.position.y+= Math.min(this.speed[2]*delta, deltay);
        else if (deltay < 0)
            this.position.y+= Math.max(-this.speed[3]*delta, deltay);
        
        this.image.setAbsolutePosition({x: this.position.x, y: this.position.y});
        
    }
});