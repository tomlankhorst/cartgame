var Environment = new Class({
    
    Extends: Drawable,
    
    stepsize: 100,
    position: 0,
    speed: 0.35,   // px/ms
    targetSpeed: 0.1,
    acceleration: 0.0005, // px/ms^2
    Map: {
        0: [0.1, 0.4, 0.6, 0.9, 1] // Water - Sand - Road - Sand - Water
    },
    SurfaceSpeeds: [0.05, 0.25, 0.6, 0.25, 0.05],
    MapIndex: [0],
    
    Surfaces: [],
    
    initialize: function(game)
    {
        this.parent(game);
        this.generate();
        
        for(var i = 0; i < 5; i++){
            this.Surfaces.push(new Kinetic.Shape(this.getShapeOptions(i)));
            this.game.animLayers.environment.add(this.Surfaces[i]);
        }
        
        for(var i = 0; i < 5; i++){
            this.Surfaces[i].moveToBottom();
        }
        
    },
    
    generate: function()
    {
        endpoint    = this.MapIndex[this.MapIndex.length - 1];
        while(endpoint < this.position + this.game.options.height + this.stepsize){
            endpoint           += this.stepsize;
            var a,b,c,d;
            a = 0.3*Math.random();
            b = Math.max(a, 0.15+0.4*Math.random());
            d = 1-0.3*Math.random();
            c = Math.min(d, 0.85-0.4*Math.random());
            this.Map[endpoint]  = [a,b,c,d,1];
            this.MapIndex.push(endpoint);
        }
    },
    
    cleanup: function()
    {
        Object.each(this.Map, function(data, position){
            if(position < this.position - 3*this.stepsize){
                delete this.Map[position];
                this.MapIndex.shift();
            }
        }.bind(this));
    },
    
    draw: function(frame)
    {
        delta = this.delta(frame.time);
        
        this.targetSpeed = this.getSpeedFromPlayer();
        
        deltaSpeed = this.targetSpeed - this.speed;
        if(deltaSpeed > 0){
            this.speed += Math.min(delta*this.acceleration, deltaSpeed);
        } else if(deltaSpeed < 0){
            this.speed += Math.max(-delta*this.acceleration, deltaSpeed);
        }
        
        this.position   += delta*this.speed;
        
        this.generate();
        this.cleanup();
        
        // Render the environment
        var environment = this;
        
        this.Surfaces[0].setSceneFunc(this.generateSceneFunc(0, environment));
        
        this.Surfaces[1].setFill('#f3d900');
        this.Surfaces[1].setStroke('#aaa');
        this.Surfaces[1].setSceneFunc(this.generateSceneFunc(1, environment));
        
        this.Surfaces[2].setFill('#cbcbcb');
        this.Surfaces[2].setStroke('#aaa');
        this.Surfaces[2].setSceneFunc(this.generateSceneFunc(2, environment));
        
        this.Surfaces[3].setFill('#f3d900');
        this.Surfaces[3].setSceneFunc(this.generateSceneFunc(3, environment));
        
        this.Surfaces[4].setSceneFunc(this.generateSceneFunc(4, environment));
        
    },
    
    generateSceneFunc: function(surface, environment)
    {
        var fn = function(context){
            
            if(environment.MapIndex.length === 0)
                return;
            
            var map_top    = environment.MapIndex[environment.MapIndex.length - 1];
            var map_btm    = environment.MapIndex[0];
            
            var x_t        = environment.game.options.width*environment.Map[map_top][surface];
            var x_b        = environment.game.options.width*environment.Map[map_btm][surface];
            
            map_top        = -(map_top - environment.position);
            map_btm        = (environment.position - map_btm + environment.game.options.height);
            
            context.beginPath();
            context.moveTo(0, map_top);
            context.lineTo(0, map_btm);
            context.lineTo(x_b, map_btm);
            
            var y = map_btm;
            for(var i = 1; i < environment.MapIndex.length - 1; i++){
                var map     = environment.Map[environment.MapIndex[i]];
                var nextmap = environment.Map[environment.MapIndex[i+1]];
                var pX, pY, ctrl1X, ctrl1Y, ctrl2X, ctrl2Y;
                
                pX      = environment.game.options.width*map[surface];
                pY      = (environment.position - environment.MapIndex[i] + environment.game.options.height);
                
                ctrl1X  = (pX+environment.game.options.width*nextmap[surface])/2;
                ctrl1Y  = pY-(environment.MapIndex[i+1] - environment.MapIndex[i])/2;
               
                context.quadraticCurveTo(pX, pY, ctrl1X, ctrl1Y);
            }
            
            context.lineTo(x_t, map_top);
            context.closePath();
            context.fillStrokeShape(this);
        };
        
        return fn;
    },
    
    getShapeOptions: function(surface)
    {
        var options = {};
        
        if(surface === 0){
            options = {
                fillLinearGradientStartPoint: {x:0, y:0},
                fillLinearGradientEndPoint: {x:this.game.options.width/3,y:0},
                fillLinearGradientColorStops: [0, '#004CB3', 1, '#8ED6FF'],
            };
        } else if(surface === 4){
            options = {
                fillLinearGradientStartPoint: {x:2*this.game.options.width/3, y:0},
                fillLinearGradientEndPoint: {x:this.game.options.width,y:0},
                fillLinearGradientColorStops: [0, '#8ED6FF', 1, '#004CB3'],
            };
        }
        
        return options;
    },
    
    getSpeedFromPlayer: function(){
        var speed   = this.SurfaceSpeeds[this.SurfaceSpeeds.length - 1];
        var player  = this.game.player; 
        var ypos    = this.position + (this.game.options.height - player.position.y);
        var xpos    = player.position.x/this.game.options.width;
        var prevClosestMapPos, nextClosestMapPos;
        
        this.MapIndex.each(function(pos, key){
            if(pos >= ypos && pos <= ypos + this.stepsize)
                nextClosestMapPos = pos;
            if(pos <= ypos && pos >= ypos - this.stepsize)
                prevClosestMapPos = pos;
        }.bind(this));
        
        var MapProgress = (ypos - prevClosestMapPos) / (this.stepsize);
       
        var setSpeed = false;
        for(var i = 0; i < 5; i++){
            var boundx_p = this.Map[prevClosestMapPos][i];
            var boundx_n = this.Map[nextClosestMapPos][i];
            var boundx = boundx_p + MapProgress*(boundx_n - boundx_p);
            if(xpos < boundx && !setSpeed){
                setSpeed = true;
                speed = this.SurfaceSpeeds[i];
                
                switch(i){
                    case 0: 
                        this.game.setImpedance(0.003, 0, 0, 100);
                        break;
                    case 1: 
                        this.game.setImpedance(0.003, 0, 0, 0);
                        break;
                    case 2: 
                        this.game.setImpedance(0.003, 0, 0, 0);
                        break;
                    case 3: 
                        this.game.setImpedance(0.003, 0, 0, 100);
                        break;
                    case 4: 
                        this.game.setImpedance(0.003, 0, 0, 0);
                        break;
                }
                
                
            }
        }
        
        return speed;
    }
    
});