var Drawable = new Class({
    
    lastTime:   0,
    game:       null,
    
    initialize: function(game){
        this.game = game;
    },
    
    delta: function(time)
    {
        var delta       = time - this.lastTime;
        this.lastTime   = time;
        return delta;
    },
    
    draw: function(frame){}
    
});